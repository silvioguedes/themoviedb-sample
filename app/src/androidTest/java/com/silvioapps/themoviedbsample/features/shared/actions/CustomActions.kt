package com.silvioapps.themoviedbsample.features.shared.actions

import android.view.View
import androidx.appcompat.widget.SearchView
import androidx.test.espresso.NoMatchingViewException
import androidx.test.espresso.PerformException
import androidx.test.espresso.UiController
import androidx.test.espresso.ViewAction
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.*
import androidx.test.espresso.util.HumanReadables
import androidx.test.espresso.util.TreeIterables
import org.hamcrest.CoreMatchers.allOf
import org.hamcrest.Matcher
import java.util.concurrent.TimeoutException

class CustomActions {
    companion object {
        fun waitId(viewId: Int): ViewAction {
            return object: ViewAction {
                override fun getConstraints(): Matcher<View> {
                    return ViewMatchers.isRoot()
                }

                override fun getDescription(): String {
                    return "wait for a specific view with id <" + viewId + ">"
                }

                override fun perform(uiController: UiController, view: View) {
                    uiController.loopMainThreadUntilIdle()
                    val viewMatcher = ViewMatchers.withId(viewId)

                    val start = System.currentTimeMillis()
                    var end = start
                    var current = 1000
                    while(end - start < current){
                        end = System.currentTimeMillis()
                        if(end - start >= current){
                            for (child in TreeIterables.breadthFirstViewTraversal(view)) {
                                // found view with required ID
                                if (viewMatcher.matches(child)) {
                                    return
                                }
                                else{
                                    current = current + 1000
                                }
                            }
                            uiController.loopMainThreadForAtLeast(50)
                        }
                    }

                    // timeout happens
                    throw PerformException.Builder()
                        .withActionDescription(this.getDescription())
                        .withViewDescription(HumanReadables.describe(view))
                        .withCause(TimeoutException())
                        .build()
                }
            }
        }

        fun submitText(text: String): ViewAction{
            return object: ViewAction{
                override fun getConstraints(): Matcher<View> {
                    return allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))
                }

                override fun getDescription(): String {
                    return "Set text and submit"
                }

                override fun perform(uiController: UiController?, view: View?) {
                    (view as SearchView).setQuery(text, true)
                }
            }
        }

        fun typeText(text: String): ViewAction{
            return object: ViewAction{
                override fun getConstraints(): Matcher<View> {
                    return allOf(isDisplayed(), isAssignableFrom(SearchView::class.java))
                }

                override fun getDescription(): String {
                    return "Set text"
                }

                override fun perform(uiController: UiController?, view: View?) {
                    (view as SearchView).setQuery(text, false)
                }
            }
        }

        /**
         * Perform action of waiting for a certain view within a single root view
         * @param matcher Generic Matcher used to find our view
         */
        fun searchFor(matcher: Matcher<View>): ViewAction {

            return object : ViewAction {

                override fun getConstraints(): Matcher<View> {
                    return isRoot()
                }

                override fun getDescription(): String {
                    return "searching for view $matcher in the root view"
                }

                override fun perform(uiController: UiController, view: View) {

                    var tries = 0
                    val childViews: Iterable<View> = TreeIterables.breadthFirstViewTraversal(view)

                    // Look for the match in the tree of childviews
                    childViews.forEach {
                        tries++
                        if (matcher.matches(it)) {
                            // found the view
                            return
                        }
                    }

                    throw NoMatchingViewException.Builder()
                        .withRootView(view)
                        .withViewMatcher(matcher)
                        .build()
                }
            }
        }
    }
}