package com.silvioapps.themoviedbsample.features.list.tests

import android.content.Intent
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.di.applications.AppInstrumentedTest
import com.silvioapps.themoviedbsample.features.list.activities.MainActivity
import com.silvioapps.themoviedbsample.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import org.junit.After
import org.junit.Before

@RunWith(AndroidJUnit4::class)
@LargeTest
class MainActivityInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
       app.setComponent()

        val intent = Intent(app, MainActivity::class.java)
        activityScenario = launchActivity(intent)
    }

    @After
    fun after(){
        FetcherListenerInstrumentedTestImpl.destroyInstance()
    }

    @Test
    fun test() {
        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))
    }
}
