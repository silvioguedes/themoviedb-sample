package com.silvioapps.themoviedbsample.features.list.implementations

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListImageFetchListenerInstrumentedTestImpl @Inject private constructor(): ListImageFetchListener {
    private var isIdle = false
    private var position = 0

    companion object{
        private var instance: ListImageFetchListenerInstrumentedTestImpl? = null

        @Synchronized
        fun getInstance(): ListImageFetchListenerInstrumentedTestImpl?{
            if(instance == null){
                instance =
                    ListImageFetchListenerInstrumentedTestImpl()
            }
            return instance
        }

        @Synchronized
        fun destroyInstance(){
            instance = null
        }
    }

    override fun beginFetching() {
        isIdle = false
    }

    override fun doneFetching() {
        isIdle = true
    }

    override fun isIdle(): Boolean{
        return isIdle
    }

    override fun setPosition(position: Int){
        this.position = position
    }

    override fun getPosition(): Int{
        return position
    }
}