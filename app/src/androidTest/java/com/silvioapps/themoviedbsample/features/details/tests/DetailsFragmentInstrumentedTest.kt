package com.silvioapps.themoviedbsample.features.list

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.di.applications.AppInstrumentedTest
import org.junit.Before
import com.silvioapps.themoviedbsample.features.list.activities.MainActivity
import com.silvioapps.themoviedbsample.features.shared.actions.CustomActions.Companion.submitText
import com.silvioapps.themoviedbsample.features.details.async_tasks.DetailsImageFetchAsyncTask
import com.silvioapps.themoviedbsample.features.shared.async_tasks.FetcherAsyncTask
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsImageFetchListenerInstrumentedTestImpl
import com.silvioapps.themoviedbsample.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import com.silvioapps.themoviedbsample.features.list.implementations.ListImageFetchListenerInstrumentedTestImpl
import com.silvioapps.themoviedbsample.features.shared.matchers.CustomMatchers.Companion.backButton
import org.hamcrest.Matchers.*
import org.junit.After

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsFragmentInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<MainActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
        app.setComponent()

        val intent = Intent(app, MainActivity::class.java)
        activityScenario = launchActivity(intent)
    }

    @After
    fun after(){
        ListImageFetchListenerInstrumentedTestImpl.destroyInstance()
        DetailsImageFetchListenerInstrumentedTestImpl.destroyInstance()
        FetcherListenerInstrumentedTestImpl.destroyInstance()
    }

    @Test
    fun test(){
        onView(withId(R.id.action_search)).perform(click())

        onView(withId(R.id.action_search)).perform(submitText("Avengers: Endgame"))

        FetcherAsyncTask(FetcherListenerInstrumentedTestImpl.getInstance()!!).execute().get()

        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        DetailsImageFetchAsyncTask(DetailsImageFetchListenerInstrumentedTestImpl.getInstance()!!).execute().get()

        onView(withId(R.id.imageView)).check(matches(isDisplayed()))

        onView(withId(R.id.releaseDateTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.releaseDateTextView)).check(matches(withText(startsWith("D"))))

        onView(withId(R.id.voteAverageTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.voteAverageTextView)).check(matches(withText(startsWith("N"))))

        onView(withId(R.id.overviewTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.overviewTextView)).check(matches(withText(startsWith("R"))))

        onView(backButton(R.id.toolBar)).perform(click())
    }
}
