package com.silvioapps.themoviedbsample.features.shared.utils

import android.view.View
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.ViewFinder
import org.hamcrest.Matcher

class TestUtils{
    companion object{
        fun getView(viewMatcher: Matcher<View>): View?{
            try{
                val viewInteraction = onView(viewMatcher)
                val finderField = viewInteraction.javaClass.getDeclaredField("viewFinder")
                finderField.isAccessible = true
                val viewFinder = finderField.get(viewInteraction) as ViewFinder
                return viewFinder.view
            }
            catch(e: Exception){
                e.printStackTrace()
            }
            return null
        }
    }
}