package com.silvioapps.themoviedbsample.features.details.implementations

import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class DetailsImageFetchListenerInstrumentedTestImpl @Inject private constructor(): DetailsImageFetchListener{
    private var isIdle = false
    private var position = 0

    companion object{
        private var instance: DetailsImageFetchListenerInstrumentedTestImpl? = null

        @Synchronized
        fun getInstance(): DetailsImageFetchListenerInstrumentedTestImpl?{
            if(instance == null){
                instance =
                    DetailsImageFetchListenerInstrumentedTestImpl()
            }
            return instance
        }

        @Synchronized
        fun destroyInstance(){
            instance = null
        }
    }

    override fun beginFetching() {
        isIdle = false
    }

    override fun doneFetching() {
        isIdle = true
    }

    override fun isIdle(): Boolean{
        return isIdle
    }

    override fun setPosition(position: Int){
        this.position = position
    }

    override fun getPosition(): Int{
        return position
    }
}