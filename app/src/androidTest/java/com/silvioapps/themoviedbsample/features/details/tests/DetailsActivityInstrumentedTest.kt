package com.silvioapps.themoviedbsample.features.list

import android.content.Intent
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.di.applications.AppInstrumentedTest
import com.silvioapps.themoviedbsample.features.details.activities.DetailsActivity
import com.silvioapps.themoviedbsample.features.list.activities.MainActivity
import com.silvioapps.themoviedbsample.features.shared.async_tasks.FetcherAsyncTask
import com.silvioapps.themoviedbsample.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import org.junit.After
import org.junit.Before

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsActivityInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<DetailsActivity>

    @Before
    fun before(){
        val app = InstrumentationRegistry.getInstrumentation().targetContext.applicationContext as AppInstrumentedTest
       app.setComponent()

        val intent = Intent(app, MainActivity::class.java)
        activityScenario = launchActivity(intent)
    }

    @After
    fun after(){
        FetcherListenerInstrumentedTestImpl.destroyInstance()
    }

    @Test
    fun test(){
        FetcherAsyncTask(FetcherListenerInstrumentedTestImpl.getInstance()!!).execute().get()

        onView(withId(R.id.recyclerView)).perform(
            RecyclerViewActions.actionOnItemAtPosition<RecyclerView.ViewHolder>(0, click()))

        onView(withId(R.id.fragmentContainerView)).check(matches(isDisplayed()))
    }
}
