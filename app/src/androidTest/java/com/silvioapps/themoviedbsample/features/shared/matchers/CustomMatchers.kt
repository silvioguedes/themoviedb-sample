package com.silvioapps.themoviedbsample.features.shared.matchers

import android.view.View
import android.widget.ImageButton
import androidx.recyclerview.widget.RecyclerView
import androidx.test.espresso.matcher.BoundedMatcher
import androidx.test.espresso.matcher.ViewMatchers
import com.silvioapps.themoviedbsample.R
import org.hamcrest.Description
import org.hamcrest.TypeSafeMatcher
import org.hamcrest.Matcher
import org.hamcrest.Matchers

class CustomMatchers{
    companion object{
        fun withIndex(matcher: Matcher<View>, index: Int): Matcher<View> {
            return object: TypeSafeMatcher<View>() {
                var currentIndex = 0
                var viewObjHash = 0

                override fun describeTo(description: Description) {
                    description.appendText("with index: ")
                    //description.appendValue(index)
                    matcher.describeTo(description)
                }

                override fun matchesSafely(view: View): Boolean {
                    //return matcher.matches(view) && currentIndex++ == index
                    if(matcher.matches(view) && currentIndex++ == index){
                        viewObjHash = view.hashCode()
                    }
                    return view.hashCode() == viewObjHash
                }
            }
        }

        fun atPosition(position: Int, itemMatcher: Matcher<View>): Matcher<View> {
            return object: BoundedMatcher<View, RecyclerView>(RecyclerView::class.java) {

                override fun describeTo(description: Description) {
                    description.appendText("has item at position " + position + ": ")
                    itemMatcher.describeTo(description)
                }

                override fun matchesSafely(view: RecyclerView): Boolean {
                    val viewHolder = view.findViewHolderForAdapterPosition(position)
                    if (viewHolder == null) {
                        // has no item on such position
                        return false
                    }
                    return itemMatcher.matches(viewHolder.itemView)
                }
            }
        }

        fun backButton(toolBarId: Int): Matcher<View>{
            return Matchers.allOf(
                Matchers.instanceOf(ImageButton::class.java),
                ViewMatchers.withParent(ViewMatchers.withId(toolBarId))
            )
        }

        fun backButton(): Matcher<View>{
            return ViewMatchers.withContentDescription(R.string.abc_action_bar_up_description)
        }
    }
}