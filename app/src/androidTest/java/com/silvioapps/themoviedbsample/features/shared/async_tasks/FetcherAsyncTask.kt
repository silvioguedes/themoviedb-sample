package com.silvioapps.themoviedbsample.features.shared.async_tasks

import android.os.AsyncTask
import com.silvioapps.themoviedbsample.features.shared.listeners.FetcherListener

class FetcherAsyncTask(val fetcherListener: FetcherListener): AsyncTask<Void, Void, Void?>() {
    override fun doInBackground(vararg p0: Void): Void? {
        while(!fetcherListener.isIdle()){}
        return null
    }

    override fun onPostExecute(result: Void?) {}

    override fun onPreExecute() {}

    override fun onProgressUpdate(vararg p0: Void) {}
}