package com.silvioapps.themoviedbsample.features.list

import android.content.Intent
import android.os.Bundle
import androidx.test.core.app.ActivityScenario
import androidx.test.core.app.launchActivity
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions.matches
import androidx.test.espresso.matcher.ViewMatchers.*
import org.junit.Test
import org.junit.runner.RunWith
import androidx.test.ext.junit.runners.AndroidJUnit4
import androidx.test.filters.LargeTest
import androidx.test.platform.app.InstrumentationRegistry
import com.silvioapps.themoviedbsample.R
import org.junit.Before
import com.silvioapps.themoviedbsample.features.details.activities.DetailsActivity
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem
import com.silvioapps.themoviedbsample.features.shared.matchers.CustomMatchers.Companion.backButton
import org.hamcrest.Matchers.*
import java.io.Serializable

@RunWith(AndroidJUnit4::class)
@LargeTest
class DetailsFragmentMockInstrumentedTest {
    lateinit var activityScenario: ActivityScenario<DetailsActivity>

    @Before
    fun before(){
        val intent = Intent(InstrumentationRegistry.getInstrumentation().targetContext, DetailsActivity::class.java)
        val bundle = Bundle()

        val responseItem = ResponseItem(
            title = "Teste",
            releaseDate = "2019-01-01",
            voteAverage = 10.0,
            overview = "Teste"
        )

        bundle.putSerializable("details", responseItem as Serializable)
        intent.putExtra("data", bundle)

        activityScenario = launchActivity(intent)
    }

    @Test
    fun test(){
        onView(withId(R.id.imageView)).check(matches(isDisplayed()))

        onView(withId(R.id.releaseDateTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.releaseDateTextView)).check(matches(withText(containsString("2019"))))

        onView(withId(R.id.voteAverageTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.voteAverageTextView)).check(matches(withText(containsString("10.0"))))

        onView(withId(R.id.overviewTextView)).check(matches(isDisplayed()))

        onView(withId(R.id.overviewTextView)).check(matches(withText(containsString("Teste"))))

        onView(backButton(R.id.toolBar)).perform(click())
    }
}
