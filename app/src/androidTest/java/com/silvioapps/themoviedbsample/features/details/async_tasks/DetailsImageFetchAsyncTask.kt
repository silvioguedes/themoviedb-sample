package com.silvioapps.themoviedbsample.features.details.async_tasks

import android.os.AsyncTask
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsImageFetchListener

class DetailsImageFetchAsyncTask(val imageFetchListener: DetailsImageFetchListener): AsyncTask<Void, Void, Void?>() {
    override fun doInBackground(vararg p0: Void): Void? {
        while(!imageFetchListener.isIdle()){}
        return null
    }

    override fun onPostExecute(result: Void?) {}

    override fun onPreExecute() {}

    override fun onProgressUpdate(vararg p0: Void) {}
}