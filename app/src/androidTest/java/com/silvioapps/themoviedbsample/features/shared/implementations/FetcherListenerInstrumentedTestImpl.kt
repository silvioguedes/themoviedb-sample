package com.silvioapps.themoviedbsample.features.shared.implementations

import com.silvioapps.themoviedbsample.features.shared.listeners.FetcherListener
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class FetcherListenerInstrumentedTestImpl @Inject private constructor(): FetcherListener {
    private var isIdle = false

    companion object{
        private var instance: FetcherListenerInstrumentedTestImpl? = null

        @Synchronized
        fun getInstance(): FetcherListenerInstrumentedTestImpl?{
            if(instance == null){
                instance = FetcherListenerInstrumentedTestImpl()
            }
            return instance
        }

        @Synchronized
        fun destroyInstance(){
            instance = null
        }
    }

    override fun beginFetching() {
        isIdle = false
    }

    override fun doneFetching() {
        isIdle = true
    }

    override fun isIdle(): Boolean{
        return isIdle
    }
}