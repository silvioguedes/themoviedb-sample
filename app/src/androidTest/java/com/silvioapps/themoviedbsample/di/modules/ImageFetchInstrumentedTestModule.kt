package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.themoviedbsample.features.list.implementations.ListImageFetchListener
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsImageFetchListenerInstrumentedTestImpl
import com.silvioapps.themoviedbsample.features.list.implementations.ListImageFetchListenerInstrumentedTestImpl
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class ImageFetchInstrumentedTestModule: ImageFetchModule() {

    @Provides
    @Singleton
    override fun providesListImageFetchListener(): ListImageFetchListener {
        return ListImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }

    @Provides
    @Singleton
    override fun providesDetailsImageFetchListener(): DetailsImageFetchListener {
        return DetailsImageFetchListenerInstrumentedTestImpl.getInstance()!!
    }
}