package com.silvioapps.themoviedbsample.di.applications

import com.silvioapps.themoviedbsample.di.components.DaggerAppComponent
import com.silvioapps.themoviedbsample.di.modules.*

class AppInstrumentedTest : App(){
    override fun setComponent(){
        DaggerAppComponent
            .builder()
            .application(this)
            .recyclerViewModule(RecyclerViewModule())
            .mainFragmentModule(MainFragmentModule())
            .apiModule(ApiModule())
            .databaseModule(DatabaseModule())
            .fetcherModule(FetcherInstrumentedTestModule())
            .detailsFragmentModule(DetailsFragmentModule())
            .imageFetchModule(ImageFetchInstrumentedTestModule())
            .build()
            .inject(this)
    }
}
