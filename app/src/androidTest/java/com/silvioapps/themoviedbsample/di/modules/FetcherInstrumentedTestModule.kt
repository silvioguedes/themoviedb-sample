package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.shared.implementations.FetcherListenerInstrumentedTestImpl
import com.silvioapps.themoviedbsample.features.shared.listeners.FetcherListener
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
class FetcherInstrumentedTestModule: FetcherModule() {

    @Provides
    @Singleton
    override fun providesFetcherListener(): FetcherListener {
        return FetcherListenerInstrumentedTestImpl.getInstance()!!
    }
}