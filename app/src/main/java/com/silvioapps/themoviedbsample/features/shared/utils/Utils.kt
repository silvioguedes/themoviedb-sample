package com.silvioapps.themoviedbsample.features.shared.utils

import android.content.Context
import android.graphics.Point
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.SearchView
import androidx.core.content.ContextCompat
import androidx.core.text.HtmlCompat
import com.silvioapps.themoviedbsample.features.shared.listeners.ViewClickListener
import javax.net.ssl.SSLContext
import com.google.android.gms.security.ProviderInstaller
import com.silvioapps.themoviedbsample.features.shared.listeners.ViewInflatedListener
import java.text.SimpleDateFormat
import java.util.*

class Utils {
    companion object{
        fun setTags(position : Int, view : View) {
            if (view is ViewGroup) {
                for (index in 0..view.getChildCount()) {
                    val nextChild : View? = view.getChildAt(index)
                    nextChild?.setTag(position)

                    if (nextChild is ViewGroup) {
                        setTags(position, nextChild)
                    }
                }
            }
        }

        fun setClickListeners(view : View, viewClickListener : ViewClickListener?, list: List<Any>) {
            if (view is ViewGroup) {
                for (index in 0..view.getChildCount()) {
                    val nextChild : View? = view.getChildAt(index)
                    nextChild?.setOnClickListener(object : View.OnClickListener {
                        override fun onClick(v : View) {
                            if (nextChild.getTag() != null) {
                                viewClickListener?.onClick(nextChild.context, nextChild, nextChild.getTag() as Int, list)
                            }
                        }
                    });

                    if (nextChild is ViewGroup) {
                        setClickListeners(nextChild, viewClickListener, list)
                    }
                }
            }
        }

        fun getScreenSize(context: Context): Point {
            val metrics = context.getResources().getDisplayMetrics()
            val width = metrics.widthPixels
            val height = metrics.heightPixels
            return Point(width, height)
        }

        fun fixSSLError(context : Context){
            try {
                ProviderInstaller.installIfNeeded(context)
                val sslContext = SSLContext.getInstance("TLSv1.2")
                sslContext.init(null, null, null)
                sslContext.createSSLEngine()
            }
            catch(e : Exception){
                e.printStackTrace()
            }
        }

        fun setQueryHintColor(searchView: SearchView, colorRes: Int){
            searchView.setQueryHint(HtmlCompat.fromHtml("<font color = " + ContextCompat.getColor(searchView.context, colorRes) + ">" + searchView.queryHint + "</font>", HtmlCompat.FROM_HTML_MODE_LEGACY))
        }

        fun onViewInflated(view: View, viewInflatedListener: ViewInflatedListener){
            view.addOnLayoutChangeListener(object: View.OnLayoutChangeListener{
                override fun onLayoutChange(
                    view: View?,
                    left: Int,
                    top: Int,
                    right: Int,
                    bottom: Int,
                    oldLeft: Int,
                    oldTop: Int,
                    oldRight: Int,
                    oldBottom: Int
                ) {
                    val width = right - left
                    val height = bottom - top
                    if(width > 0 && height > 0) {
                        viewInflatedListener.onInflated()
                        view?.removeOnLayoutChangeListener(this)
                    }
                }
            })
        }

        @JvmStatic
        fun formatDate(dateToFormat: String?, format: String, locale: Locale): String?{
                var simpleDateFormat: SimpleDateFormat? = null
                try {
                    simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH)
                    simpleDateFormat.setLenient(true)
                }
                catch(e: IllegalArgumentException){
                    e.printStackTrace();
                }

                var dateFormatted: String? = dateToFormat
                var date: Date? = null
                if(simpleDateFormat != null) {
                    try {
                        date = simpleDateFormat.parse(dateToFormat)
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                }

                simpleDateFormat = SimpleDateFormat(format, locale)
                try {
                    dateFormatted = simpleDateFormat.format(date)
                } catch (e: Exception) {
                    e.printStackTrace()
                }

                return dateFormatted
        }
    }
}
