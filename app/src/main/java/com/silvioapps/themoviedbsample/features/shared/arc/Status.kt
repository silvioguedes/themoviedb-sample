package com.silvioapps.themoviedbsample.features.shared.arc

enum class Status {
    UNKNOWN_CODE,
    SUCCESS,
    ERROR,
    LOADING
}