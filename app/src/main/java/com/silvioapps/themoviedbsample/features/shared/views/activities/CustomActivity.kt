package com.silvioapps.themoviedbsample.features.shared.views.activities

import androidx.fragment.app.Fragment
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar

open class CustomActivity : AppCompatActivity() {
    protected fun attachFragment(resId : Int, fragment : Fragment){
        val fragmentTransaction = supportFragmentManager.beginTransaction()
        val fragmentById = supportFragmentManager.findFragmentById(resId)
        if(fragmentById == null && !isFinishing){
            fragmentTransaction.add(resId, fragment)
            fragmentTransaction.commit()
        }
    }

    protected fun showToolbar(toolbar : Toolbar?, title : String) {
        if(toolbar != null) {
            setSupportActionBar(toolbar)
        }
        setTitle(title)
    }

    protected fun showBackButton(toolbar : Toolbar?, title : String) {
        if(toolbar != null) {
            setSupportActionBar(toolbar)
        }
        getSupportActionBar()?.setDisplayHomeAsUpEnabled(true)
        getSupportActionBar()?.setDisplayShowHomeEnabled(true)
        setTitle(title)
    }
}
