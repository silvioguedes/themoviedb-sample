package com.silvioapps.themoviedbsample.features.list.services

import androidx.lifecycle.LiveData
import com.silvioapps.themoviedbsample.features.shared.arc.ApiResponse
import com.silvioapps.themoviedbsample.constants.Constants
import com.silvioapps.themoviedbsample.features.list.models.ListResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface ListSearchService {
    @GET(Constants.SEARCH)
    fun getLiveDataApiResponse(@Query("api_key") apiKey: String, @Query("query") query: String, @Query("page") page: Int): LiveData<ApiResponse<ListResponse>>
}