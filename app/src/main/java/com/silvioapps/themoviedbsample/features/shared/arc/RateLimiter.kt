package com.silvioapps.themoviedbsample.features.shared.arc

import android.os.SystemClock
import androidx.collection.arrayMapOf
import java.util.concurrent.TimeUnit

/**
 * Utility class that decides whether we should fetch some data or not.
 */
class RateLimiter<KEY>(var timeout: Long, timeUnit: TimeUnit) {
    private val timestamps = arrayMapOf<KEY, Long>()

    init{
        this.timeout = timeUnit.toMillis(timeout)
    }

    fun shouldFetch(): Boolean{
        return shouldFetch(null)
    }

    fun shouldFetch(key: KEY?): Boolean{
        val lastFetched = timestamps.get(key)
        val now = now()
        if (lastFetched == null) {
            timestamps.put(key, now);
            return true;
        }
        if (now - lastFetched > timeout) {
            timestamps.put(key, now);
            return true;
        }

        return false;
    }

    private fun now(): Long {
        return SystemClock.uptimeMillis()
    }

    fun reset(key: KEY) {
        timestamps.remove(key)
    }
}