package com.silvioapps.themoviedbsample.features.shared.services

import com.google.gson.Gson
import com.silvioapps.themoviedbsample.features.shared.factories.LiveDataCallAdapterFactory
import java.util.concurrent.TimeUnit
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory

class ServiceGenerator {
    companion object{
        fun <T> createService(apiBaseUrl : String, timeout : Long, withLiveData: Boolean, serviceClass : Class<T>) : T {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.setLevel(HttpLoggingInterceptor.Level.BODY)

            val httpClient = OkHttpClient.Builder().readTimeout(timeout, TimeUnit.SECONDS)
            httpClient?.addInterceptor(loggingInterceptor)

            val retrofit: Retrofit
            if(withLiveData) {
                retrofit = Retrofit.Builder()
                    .baseUrl(apiBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .addCallAdapterFactory(LiveDataCallAdapterFactory())
                    .client(httpClient.build())
                    .build()
            }
            else{
                retrofit = Retrofit.Builder()
                    .baseUrl(apiBaseUrl)
                    .addConverterFactory(GsonConverterFactory.create(Gson()))
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .client(httpClient.build())
                    .build()
            }

            return retrofit.create(serviceClass)
        }
    }
}
