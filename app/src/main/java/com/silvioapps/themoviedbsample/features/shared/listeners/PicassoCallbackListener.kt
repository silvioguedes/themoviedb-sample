package com.silvioapps.themoviedbsample.features.shared.listeners

import android.widget.ImageView

interface PicassoCallbackListener {
    fun onSuccess(imageView: ImageView, position: Int)
    fun onError(imageView: ImageView, position: Int)
}