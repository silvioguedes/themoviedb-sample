package com.silvioapps.themoviedbsample.features.details.implementations

import android.widget.ImageView
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.features.shared.listeners.PicassoCallbackListener
import com.silvioapps.themoviedbsample.features.shared.listeners.ViewInflatedListener
import com.silvioapps.themoviedbsample.features.shared.utils.Utils
import com.squareup.picasso.Picasso
import javax.inject.Inject

interface DetailsPicassoCallbackListener : PicassoCallbackListener {
    override fun onSuccess(imageView: ImageView, position: Int) {}
    override fun onError(imageView: ImageView, position: Int) {}
}

class DetailsPicassoCallbackListenerImpl @Inject constructor(val imageFetchListener: DetailsImageFetchListener): DetailsPicassoCallbackListener{
    override fun onSuccess(imageView: ImageView, position: Int) {
        Utils.onViewInflated(imageView, object: ViewInflatedListener{
            override fun onInflated() {
                imageFetchListener.doneFetching()
            }
        })
    }

    override fun onError(imageView: ImageView, position: Int) {
        Picasso.with(imageView.context).load(R.drawable.no_image_available).into(imageView)
        Utils.onViewInflated(imageView, object: ViewInflatedListener{
            override fun onInflated() {
                imageFetchListener.doneFetching()
            }
        })
    }
}