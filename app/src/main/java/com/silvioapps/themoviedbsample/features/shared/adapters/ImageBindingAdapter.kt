package com.silvioapps.themoviedbsample.features.shared.adapters

import androidx.databinding.BindingAdapter
import android.widget.ImageView
import com.silvioapps.themoviedbsample.features.shared.listeners.PicassoCallbackListener
import com.squareup.picasso.Callback

import com.squareup.picasso.Picasso

@BindingAdapter(value=["android:url", "callback", "position"], requireAll = false)
fun setImage(imageView : ImageView, url : String?, callback: PicassoCallbackListener?, position: Int?) {
    var pos = 0
    if(position != null) {
        pos = position
    }

    Picasso.with(imageView.context).load(url).into(imageView, object : Callback {
        override fun onSuccess() {
            callback?.onSuccess(imageView, pos)
        }

        override fun onError() {
            callback?.onError(imageView, pos)
        }
    })
}

