package com.silvioapps.themoviedbsample.features.list.implementations

import android.widget.ImageView
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.features.shared.listeners.PicassoCallbackListener
import com.silvioapps.themoviedbsample.features.shared.listeners.ViewInflatedListener
import com.silvioapps.themoviedbsample.features.shared.utils.Utils
import com.squareup.picasso.Picasso
import javax.inject.Inject

interface ListPicassoCallbackListener : PicassoCallbackListener {
    override fun onSuccess(imageView: ImageView, position: Int) {}
    override fun onError(imageView: ImageView, position: Int) {}
}

class ListPicassoCallbackListenerImpl @Inject constructor(val imageFetchListener: ListImageFetchListener): ListPicassoCallbackListener{
    override fun onSuccess(imageView: ImageView, position: Int) {
        Utils.onViewInflated(imageView, object: ViewInflatedListener{
            override fun onInflated() {
                if(position == imageFetchListener.getPosition()) {
                    imageFetchListener.doneFetching()
                }
            }
        })
    }

    override fun onError(imageView: ImageView, position: Int) {
        Picasso.with(imageView.context).load(R.drawable.no_image_available).into(imageView)
        Utils.onViewInflated(imageView, object: ViewInflatedListener{
            override fun onInflated() {
                if(position == imageFetchListener.getPosition()) {
                    imageFetchListener.doneFetching()
                }
            }
        })
    }
}