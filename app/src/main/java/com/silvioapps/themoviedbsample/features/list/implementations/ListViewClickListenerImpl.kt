package com.silvioapps.themoviedbsample.features.list.implementations

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.View
import com.silvioapps.themoviedbsample.features.details.activities.DetailsActivity
import com.silvioapps.themoviedbsample.features.shared.listeners.ViewClickListener
import javax.inject.Inject
import java.io.Serializable

interface ListViewClickListener : ViewClickListener {
    override fun onClick(context : Context, view : View, position : Int, list: List<Any>)
}

class ListViewClickListenerImpl @Inject constructor(): ListViewClickListener{
    override fun onClick(context: Context, view: View, position: Int, list: List<Any>) {
        val intent = Intent(context, DetailsActivity::class.java)

        val bundle = Bundle()
        bundle.putSerializable("details", list[position] as Serializable)

        intent.putExtra("data", bundle)
        context.startActivity(intent)
    }
}