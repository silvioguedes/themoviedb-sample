package com.silvioapps.themoviedbsample.features.list.models

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import java.io.Serializable
import javax.inject.Singleton

@Singleton
@Entity
data class ListResponse(
	@PrimaryKey(autoGenerate = true)
	var id: Int? = null,

	@field:SerializedName("page")
	var page: Int? = null,

	@field:SerializedName("total_pages")
	var totalPages: Int? = null,

	@field:SerializedName("results")
	var results: List<ResponseItem>? = null,

	@field:SerializedName("total_results")
	var totalResults: Int? = null
): Serializable