package com.silvioapps.themoviedbsample.features.shared.listeners

interface FetcherListener {
    fun beginFetching()
    fun doneFetching()
    fun isIdle(): Boolean
}