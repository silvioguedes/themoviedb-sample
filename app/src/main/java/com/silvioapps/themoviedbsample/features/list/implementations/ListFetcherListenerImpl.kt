package com.silvioapps.themoviedbsample.features.list.implementations

import com.silvioapps.themoviedbsample.features.shared.listeners.FetcherListener
import javax.inject.Inject

class ListFetcherListenerImpl @Inject constructor(): FetcherListener {
    override fun beginFetching(){}
    override fun doneFetching(){}
    override fun isIdle(): Boolean{
        return true
    }
}