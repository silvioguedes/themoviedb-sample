package com.silvioapps.themoviedbsample.features.list.repositories

import java.util.concurrent.TimeUnit
import androidx.lifecycle.LiveData
import com.silvioapps.themoviedbsample.constants.Constants
import com.silvioapps.themoviedbsample.features.shared.arc.AppExecutors
import com.silvioapps.themoviedbsample.features.shared.arc.NetworkBoundResource
import com.silvioapps.themoviedbsample.features.shared.arc.RateLimiter
import com.silvioapps.themoviedbsample.features.shared.arc.Resource
import com.silvioapps.themoviedbsample.features.list.daos.ListDao
import com.silvioapps.themoviedbsample.features.list.models.ListResponse
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem
import com.silvioapps.themoviedbsample.features.list.services.ListSearchService
import javax.inject.Inject
import javax.inject.Singleton

@Singleton
class ListRepository @Inject constructor(private val appExecutors: AppExecutors, private val dao: ListDao,
                                         private val searchService: ListSearchService){
    private val rateLimit = RateLimiter<String>(Constants.RATE_TIMEOUT, TimeUnit.MINUTES)

    fun setResponseItemList(value: List<ResponseItem>){
        dao.setResponseItemList(value)
    }

    fun getLiveDataResorceResponseItemList(apiKey: String, search: String, page: Int): LiveData<Resource<List<ResponseItem>>> {
        return object : NetworkBoundResource<List<ResponseItem>, ListResponse>(appExecutors) {
            override fun saveCallResult(item: ListResponse) {
                dao.setResponseItemList(item.results!!)
            }

            override fun shouldFetch(data: List<ResponseItem>?): Boolean {
                return data == null || rateLimit.shouldFetch(search)
            }

            override fun loadFromDb(): LiveData<List<ResponseItem>> {
                return dao.getLiveDataResponseItemList(search)
            }

            override fun createCall() = searchService.getLiveDataApiResponse(apiKey, search, page)

            override fun onFetchFailed() {}
            
        }.asLiveData()
    }
}

