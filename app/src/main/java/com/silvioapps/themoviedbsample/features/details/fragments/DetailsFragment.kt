package com.silvioapps.themoviedbsample.features.details.fragments

import android.content.Context
import androidx.databinding.DataBindingUtil
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.res.ResourcesCompat
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.databinding.FragmentDetailsBinding
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsPicassoCallbackListener
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem
import com.silvioapps.themoviedbsample.features.shared.fragments.CustomFragment
import com.silvioapps.themoviedbsample.features.shared.utils.Utils
import dagger.android.support.AndroidSupportInjection
import javax.inject.Inject

class DetailsFragment @Inject constructor(): CustomFragment() {
    private lateinit var fragmentDetailsBinding : FragmentDetailsBinding
    @Inject lateinit var detailsPicassoCallbackListener: DetailsPicassoCallbackListener

    override fun onCreateView(layoutInflater : LayoutInflater, viewGroup : ViewGroup?, bundle : Bundle?) : View? {
        fragmentDetailsBinding = DataBindingUtil.inflate(layoutInflater, R.layout.fragment_details, viewGroup, false)

        val responseItem = arguments?.getSerializable("details") as ResponseItem
        fragmentDetailsBinding.responseItem = responseItem
        fragmentDetailsBinding.callback = detailsPicassoCallbackListener
        fragmentDetailsBinding.imageView.apply{
            layoutParams.height = (Utils.getScreenSize(context).y / ResourcesCompat.getFloat(context.resources, R.dimen.details_size_percent)).toInt()
        }

        showBackButton(fragmentDetailsBinding.toolBar, responseItem.title!!)

        return fragmentDetailsBinding.root
    }

    override fun onAttach(context: Context){
        AndroidSupportInjection.inject(this)
        super.onAttach(context)
    }
}
