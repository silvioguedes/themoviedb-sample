package com.silvioapps.themoviedbsample.features.list.view_models

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import com.silvioapps.themoviedbsample.features.shared.arc.Resource
import com.silvioapps.themoviedbsample.features.list.repositories.ListRepository
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem
import javax.inject.Inject;

class ListViewModel @Inject constructor(private var repository: ListRepository): ViewModel() {

    fun setResponseItemList(value: List<ResponseItem>){
        repository.setResponseItemList(value)
    }

    fun getLiveDataResorceResponseItemList(apiKey: String, search: String, page: Int): LiveData<Resource<List<ResponseItem>>> {
        return repository.getLiveDataResorceResponseItemList(apiKey, search, page)
    }
}