package com.silvioapps.themoviedbsample.features.shared.listeners

interface ViewInflatedListener {
    fun onInflated()
}
