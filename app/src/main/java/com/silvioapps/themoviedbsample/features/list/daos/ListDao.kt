package com.silvioapps.themoviedbsample.features.list.daos

import androidx.lifecycle.LiveData
import androidx.room.*
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem

@Dao
abstract class ListDao{
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    abstract fun setResponseItemList(value: List<ResponseItem>)

    @Query("SELECT * FROM ResponseItem WHERE title LIKE :search")
    abstract fun getLiveDataResponseItemList(search: String): LiveData<List<ResponseItem>>
}