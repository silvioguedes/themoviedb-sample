package com.silvioapps.themoviedbsample.features.list.services

import com.silvioapps.themoviedbsample.constants.Constants
import com.silvioapps.themoviedbsample.features.list.models.ListResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Query

interface ListService {
    @GET(Constants.LIST)
    fun getResponse(@Query("api_key") apiKey: String, @Query("page") page: Int): Observable<ListResponse>
}