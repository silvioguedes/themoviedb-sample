package com.silvioapps.themoviedbsample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class IntConverter{

    @TypeConverter
    fun from(value: List<Int>): String{
        val gson = Gson()
        val type = object: TypeToken<List<Int>>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): List<Int>{
        val gson = Gson()
        val type = object: TypeToken<List<Int>>(){}.type
        return gson.fromJson(value, type)
    }
}