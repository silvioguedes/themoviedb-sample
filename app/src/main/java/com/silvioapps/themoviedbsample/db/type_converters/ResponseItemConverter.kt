package com.silvioapps.themoviedbsample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem

class ResponseItemConverter{

    @TypeConverter
    fun from(value: List<ResponseItem>): String{
        val gson = Gson()
        val type = object: TypeToken<List<ResponseItem>>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): List<ResponseItem>{
        val gson = Gson()
        val type = object: TypeToken<List<ResponseItem>>(){}.type
        return gson.fromJson(value, type)
    }
}