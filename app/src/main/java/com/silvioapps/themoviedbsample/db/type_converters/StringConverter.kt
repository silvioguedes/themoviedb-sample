package com.silvioapps.themoviedbsample.db.type_converters

import androidx.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken

class StringConverter{

    @TypeConverter
    fun from(value: List<String>): String{
        val gson = Gson()
        val type = object: TypeToken<List<String>>(){}.type
        return gson.toJson(value, type)
    }

    @TypeConverter
    fun to(value: String): List<String>{
        val gson = Gson()
        val type = object: TypeToken<List<String>>(){}.type
        return gson.fromJson(value, type)
    }
}