package com.silvioapps.themoviedbsample.db.databases

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.silvioapps.themoviedbsample.features.list.daos.ListDao
import com.silvioapps.themoviedbsample.db.type_converters.*
import com.silvioapps.themoviedbsample.features.list.models.*

@TypeConverters(StringConverter::class, IntConverter::class, ResponseItemConverter::class)
@Database(entities = [ListResponse::class, ResponseItem::class],
    version = 4, exportSchema = false)
abstract class AppDatabase: RoomDatabase() {
    abstract fun listDao(): ListDao
}