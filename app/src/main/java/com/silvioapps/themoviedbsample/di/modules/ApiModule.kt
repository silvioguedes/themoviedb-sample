package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.constants.Constants
import com.silvioapps.themoviedbsample.features.list.services.ListSearchService
import com.silvioapps.themoviedbsample.features.list.services.ListService
import com.silvioapps.themoviedbsample.features.shared.services.ServiceGenerator
import dagger.Module
import dagger.Provides

@Module
open class ApiModule{

    @Provides
    open fun providesListService(): ListService {
        return ServiceGenerator.createService(Constants.API_BASE_URL, Constants.API_TIMEOUT, true, ListService::class.java)
    }

    @Provides
    open fun providesListSearchService(): ListSearchService {
        return ServiceGenerator.createService(Constants.API_BASE_URL, Constants.API_TIMEOUT, true, ListSearchService::class.java)
    }
}