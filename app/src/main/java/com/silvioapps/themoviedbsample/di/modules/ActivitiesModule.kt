package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.details.activities.DetailsActivity
import com.silvioapps.themoviedbsample.features.list.activities.MainActivity
import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class ActivitiesModule {
    @ContributesAndroidInjector
    abstract fun contributesMainActivity(): MainActivity

    @ContributesAndroidInjector
    abstract fun contributesDetailsActivity(): DetailsActivity
}
