package com.silvioapps.themoviedbsample.di.components

import android.app.Application
import com.silvioapps.themoviedbsample.di.applications.App
import com.silvioapps.themoviedbsample.di.modules.*
import javax.inject.Singleton
import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule

@Singleton
@Component(modules = [AndroidInjectionModule::class, AndroidSupportInjectionModule::class, AppModule::class,
    RecyclerViewModule::class, MainFragmentModule::class, ViewModelsModule::class, DatabaseModule::class, ApiModule::class,
    FetcherModule::class, DetailsFragmentModule::class, ImageFetchModule::class])
interface AppComponent: AndroidInjector<Any>{

    @Component.Builder
    interface Builder{
        @BindsInstance
        fun application(application: Application): Builder
        fun recyclerViewModule(recyclerViewModule: RecyclerViewModule): Builder
        fun mainFragmentModule(mainFragmentModule: MainFragmentModule): Builder
        fun apiModule(apiModule: ApiModule): Builder
        fun databaseModule(databaseModule: DatabaseModule): Builder
        fun fetcherModule(fetcherModule: FetcherModule): Builder
        fun detailsFragmentModule(detailsFragmentModule: DetailsFragmentModule): Builder
        fun imageFetchModule(imageFetchModule: ImageFetchModule): Builder
        fun build(): AppComponent
    }

    fun inject(application: App)
}
