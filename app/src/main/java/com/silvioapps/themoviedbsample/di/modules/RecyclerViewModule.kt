package com.silvioapps.themoviedbsample.di.modules

import android.content.Context
import androidx.core.content.res.ResourcesCompat
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.silvioapps.themoviedbsample.R
import com.silvioapps.themoviedbsample.features.shared.utils.Utils
import dagger.Module
import dagger.Provides

@Module
open class RecyclerViewModule{

    @Provides
    open fun providesLinearLayoutManager(context: Context): LinearLayoutManager {
        return object: LinearLayoutManager(context){
            override fun checkLayoutParams(layoutParams: RecyclerView.LayoutParams): Boolean {
                layoutParams.height = (Utils.getScreenSize(context).y / ResourcesCompat.getFloat(context.resources, R.dimen.row_size_percent)).toInt()
                return true
            }
        }
    }

    @Provides
    open fun providesDefaultItemAnimator(): DefaultItemAnimator {
        return DefaultItemAnimator()
    }
}