package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.details.implementations.DetailsImageFetchListener
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsPicassoCallbackListener
import com.silvioapps.themoviedbsample.features.details.implementations.DetailsPicassoCallbackListenerImpl
import dagger.Module
import dagger.Provides

@Module
open class DetailsFragmentModule{

    @Provides
    open fun providesDetailsPicassoCallbackListener(imageFetchListener: DetailsImageFetchListener): DetailsPicassoCallbackListener {
        return DetailsPicassoCallbackListenerImpl(imageFetchListener)
    }
}