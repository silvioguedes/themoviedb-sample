package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.list.adapters.MainListAdapter
import com.silvioapps.themoviedbsample.features.list.implementations.*
import com.silvioapps.themoviedbsample.features.list.models.ResponseItem
import dagger.Module
import dagger.Provides

@Module
open class MainFragmentModule{

    @Provides
    open fun providesMutableListOfResponseItem(): MutableList<ResponseItem>{
        return mutableListOf<ResponseItem>()
    }

    @Provides
    open fun providesMainListAdapter(list: MutableList<ResponseItem>, viewClickListener : ListViewClickListener, callback: ListPicassoCallbackListener): MainListAdapter {
        return MainListAdapter(list, viewClickListener, callback)
    }

    @Provides
    open fun providesListViewClickListener(): ListViewClickListener{
        return ListViewClickListenerImpl()
    }

    @Provides
    open fun providesListPicassoCallbackListener(imageFetchListener: ListImageFetchListener): ListPicassoCallbackListener {
        return ListPicassoCallbackListenerImpl(imageFetchListener)
    }
}