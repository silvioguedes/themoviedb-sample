package com.silvioapps.themoviedbsample.di.modules

import android.app.Application
import androidx.room.Room
import com.silvioapps.themoviedbsample.features.list.daos.ListDao
import com.silvioapps.themoviedbsample.db.databases.AppDatabase
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class DatabaseModule{

    @Singleton
    @Provides
    open fun providesAppDatabase(application: Application): AppDatabase {
        return Room
            .databaseBuilder(application, AppDatabase::class.java, "database.db")
            .fallbackToDestructiveMigration()
            .build()
    }

    @Singleton
    @Provides
    open fun providesListDao(db: AppDatabase): ListDao {
        return db.listDao()
    }
}

