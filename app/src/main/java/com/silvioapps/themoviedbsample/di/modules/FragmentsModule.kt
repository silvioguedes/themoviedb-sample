package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.details.fragments.DetailsFragment
import com.silvioapps.themoviedbsample.features.list.fragments.MainFragment

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Module
abstract class FragmentsModule {
    @ContributesAndroidInjector
    abstract fun contributesMainFragment(): MainFragment

    @ContributesAndroidInjector
    abstract fun contributesDetailsFragment(): DetailsFragment
}
