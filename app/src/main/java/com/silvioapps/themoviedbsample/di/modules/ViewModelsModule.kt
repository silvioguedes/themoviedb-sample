package com.silvioapps.themoviedbsample.di.modules

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.silvioapps.themoviedbsample.features.list.view_models.ListViewModel
import com.silvioapps.themoviedbsample.features.shared.factories.ViewModelFactory
import com.silvioapps.themoviedbsample.features.shared.annotations.ViewModelKey
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Module
abstract class ViewModelsModule{

    @Binds
    abstract fun bindViewModelFactory(viewModelFactory: ViewModelFactory): ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ListViewModel::class)
    abstract fun bindListViewModel(viewModel: ListViewModel): ViewModel
}

