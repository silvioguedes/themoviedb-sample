package com.silvioapps.themoviedbsample.di.modules

import com.silvioapps.themoviedbsample.features.list.implementations.ListFetcherListenerImpl
import com.silvioapps.themoviedbsample.features.shared.listeners.FetcherListener
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

@Module
open class FetcherModule {

    @Provides
    @Singleton
    open fun providesFetcherListener(): FetcherListener {
        return ListFetcherListenerImpl()
    }
}