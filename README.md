# themoviedb-sample

# README #

Estes documento README tem como objetivo fornecer as informações necessárias para realização do projeto TheMovieDB Sample.

### ESCOPO DO PROJETO ###

* Plataforma: Kotlin Android (é… o teste vai ser em kotlin, meu parceiro!)

Digamos que você faça parte de uma empresa de desenvolvimento de aplicativos. E um dos clientes desta empresa curte muito essa parada de cinema, comer pipoca, enfim… filmes em geral. E ele pediu um app com  informações sobre filmes. Bora ver quais são essas infos:

* You do not talk about fight club!
* Quando o app abrir, ele deverá listar os 30 filmes mais populares segundo a api sugerida no final
* Conforme o usuário for scrollanado (essa palavra existe???) a lista, ela deve pedir mais resultados para a api os próximos 30, e assim por diante
* Outra funcionalidade que seria interessante é a de busca, com o mesmo esquema de paginação e busca com debounce (o debounce é opcional… mas se tu fizer, será um diferencial!)
* Quais informações são interessantes de se mostrar na listagem? Estas aqui ó: Um poster do filme, a nota dele, genero, ano de lançamento, titulo, e alguma área clicável para ir para os detalhes dele.
* A tela de detalhes deve ter as mesmas informações plus uma imagem de fundo, dados mais detalhados do filme, diretor, atores, informações de lançamento, tempo, orçamento, quanto foi arrecadado com bilheteria, enfim…
* Você deverá usar o git… Isso é mandatório porque utilizaremos o repo para fazer sua avaliação! =D
* No readme do seu git, precisamos dos seguintes dados: apis que você tenha utilizado, arquitetura,  e qualquer informação que você julgue relevante!
* Fim!

É este o teste, easy peasy lemon squeezy!

A api que deve ser utilizada é a seguinte: https://www.themoviedb.org/documentation/api

Vou dar uma dica aqui ó… shh num conta pra ninguém… Mas você vai ter que fazer um cadastro nessa api… Só avisando…
Você tem 4 dias a contar do recebimento do teste para entrega-lo.

Algumas considerações:
Será um plus se você fizer/utilizar algumas dessas coisinhas aqui ó:
* RxOrganização de código e de recursos
* Testes (dá uma atenção maior para este aqui… =P)

### Justificativa das Bibliotecas usadas: ###

* Para testes unitários e instrumentados:
    androidTestImplementation 'androidx.test.ext:junit-ktx:1.1.1'
    androidTestImplementation 'androidx.test:runner:1.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-core:3.2.0'
    testImplementation 'junit:junit:4.12'
    testImplementation 'androidx.test:core:1.2.0'
    testImplementation 'androidx.test.ext:junit:1.1.1'
    testImplementation 'androidx.test.ext:truth:1.2.0'
    testImplementation 'androidx.test.espresso:espresso-core:3.2.0'
    testImplementation 'androidx.test.espresso:espresso-intents:3.2.0'
    testImplementation 'org.robolectric:robolectric:4.3'

* Para RecyclerViewActions:
    testImplementation 'androidx.test.espresso:espresso-contrib:3.2.0'
    androidTestImplementation 'androidx.test.espresso:espresso-contrib:3.2.0'

* Para método launchActivity() de ActivityScenario:
    androidTestImplementation 'androidx.test:core-ktx:1.2.0'

* Para consumo de API:
    implementation 'com.squareup.retrofit2:retrofit:2.4.0'
    implementation 'com.squareup.retrofit2:converter-gson:2.3.0'
    implementation 'com.squareup.okhttp3:logging-interceptor:3.4.0'
    implementation 'com.squareup.okhttp3:okhttp:3.11.0'

* Para RecyclerView (lista):
    implementation 'androidx.recyclerview:recyclerview:1.1.0-beta04'

* Para Picasso (cache e exibição de imagens):
    implementation 'com.squareup.picasso:picasso:2.5.2'

* Para fragments (AndroidX):
    implementation 'androidx.fragment:fragment:1.2.0-alpha04'
    implementation 'androidx.fragment:fragment-ktx:1.2.0-alpha04'

* Para Dagger 2 (injeção de dependências):
    implementation 'com.google.dagger:dagger:2.24'
    kapt 'com.google.dagger : dagger-compiler:2.24'
    implementation 'com.google.dagger:dagger-android:2.24'
    implementation 'com.google.dagger:dagger-android-support:2.24'
    kapt 'com.google.dagger:dagger-android-processor:2.24'

* Para views de Material Design:
    implementation 'com.google.android.material:material:1.0.0'

* Para RxKotlin:
    implementation 'io.reactivex.rxjava2:rxkotlin:2.4.0'
    implementation 'io.reactivex.rxjava2:rxandroid:2.1.1'

* Para RxRetrofit:
    implementation 'com.squareup.retrofit2:adapter-rxjava2:2.4.0'

* Para ARC (Android Archtecture Components):
    implementation 'android.arch.lifecycle:extensions:1.1.1'
    kapt "android.arch.lifecycle:compiler:1.1.1"

* Para Room (banco de dados):
    implementation 'android.arch.persistence.room:runtime:1.1.1'
    implementation 'android.arch.persistence.room:rxjava2:1.1.1'
    kapt "android.arch.persistence.room:compiler:1.1.1"

* Para coroutines:
    implementation 'org.jetbrains.kotlinx:kotlinx-coroutines-core:1.3.2'

* Para erro de SSL: (https://stackoverflow.com/questions/29916962/javax-net-ssl-sslhandshakeexception-javax-net-ssl-sslprotocolexception-ssl-han)
    implementation 'com.google.android.gms:play-services-auth:17.0.0'

### O que eu faria caso tivesse mais tempo: ###

* Uma tela de Favoritos em que o usuário poderia ver a lista de filmes favoritados por ele.

### Arquitetura utilizada: ###
* MVVM (Model-View-ViewModel)

### Como executar a aplicação: ###

* Permita, através das configurações do Android, que o dispositivo instale aplicativos de "Fontes Desconhecidas" ou que não necessite ser analisado pelo Play Protect;
* Transfira o arquivo .apk da pasta "apk" do projeto para o seu dispositivo;
* Na pasta de arquivos do dispositivo, ache o arquivo .apk e execute-o;
* Na tela de apresentação das permissões necessárias que o aplicativo necessita, clique em "Instalar";
* Inicie o aplicativo a partir do menu de aplicações do Android, clicando em seu ícone.


